# To build the Docker image (run this only once)
sudo docker build -t source-extractor .

# To run the source-extractor, you can take "sample_input" as <input_folder>
(sudo) ./run_docker.sh extract <input_folder>

# To evaluate the model's performance, the folder input_labeled contains 363 annotated documents and is provided upon request
Install python3 

pip3 install sklearn

(sudo) ./run_docker.sh evaluate input_labeled

Performance report:

SOURCE-PRIM

F1 score 0.971039182282794

Recall 0.9595959595959596

Precision 0.9827586206896551

SOURCE-SEC

F1 score 0.8920863309352518

Recall 0.8378378378378378

Precision 0.9538461538461539

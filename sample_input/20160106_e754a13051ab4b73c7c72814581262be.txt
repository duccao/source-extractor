France/Monde - Mise à jour : 08:29
Lutte antiterroriste: un projet de loi pour durcir le code pénal
06/01/2016 08:29
suivante précédente
Policiers, parquets et préfets seront les grands bénéficiaires d'un projet de loi bientôt présenté en Conseil des ministres, qui veut durcir le code pénal dans le but de mieux lutter contre le terrorisme après les attentats de novembre.
Les grandes lignes du projet avaient été présentées le 23 décembre par la garde des Sceaux Christiane Taubira, qui souhaitait alors qu'il soit examiné en février en Conseil des ministres.
Selon le texte, dévoilé mardi par Le Monde et dont l'AFP a pu consulter les principaux points, il s'agit d'adapter "notre dispositif législatif de lutte contre le crime organisé (...) et le terrorisme" afin de "renforcer de façon pérenne les outils et moyens mis à la disposition des autorités administratives et judiciaires", en dehors du cadre temporaire de l'état d'urgence instauré après les attentats parisiens du 13 novembre qui ont fait 130 morts.
USAGE DES ARMES DES POLICIERS
Parmi les dispositions les plus marquantes figure un assouplissement des règles d'engagement armé des policiers, une mesure réclamée de longue date par les forces de l'ordre.
Le projet de loi dispose ainsi le principe d'une "irresponsabilité pénale" en raison de "l'état de nécessité" pour tout fonctionnaire de police ou gendarme qui "hors cas de légitime défense fait un usage de son arme rendu absolument nécessaire pour mettre hors d'état de nuire une personne venant de commettre un ou plusieurs homicides volontaires et dont il existe des raisons sérieuses et actuelles de penser qu'elle est susceptible de réitérer ces crimes dans un temps très voisin des premiers actes".
RETOUR DE JIHAD
Une autre disposition vise les personnes soupçonnées d'avoir fait le jihad en Syrie ou en Irak et de vouloir commettre des attentats en France.
Elle renforce le contrôle administratif des personnes "dont il existe des raisons sérieuses de penser qu'elles ont accompli (...) des déplacements à l'étranger ayant pour objet la participation à des activités terroristes (...) dans des conditions susceptibles de les conduire à porter atteinte à la sécurité publique lors de leur retour sur le territoire français".
Ces personnes contre lesquelles il n'existe pas aujourd'hui d'éléments suffisants pour les mettre en examen pourraient dans ce cadre être assignées à résidence ou subir des contrôles administratifs, des mesures décidées par le ministère de l'Intérieur.
Ces mesures pourraient être suspendues si la personne se soumet à une action de réinsertion et d'acquisition des "valeurs de citoyenneté" dans un centre habilité.
FOUILLES ET PERQUISITIONS DE NUIT
Une autre disposition élargit la possibilité offerte aux policiers et gendarmes de procéder à des fouilles de bagages et véhicules, sous l'autorité du préfet et non plus du procureur, "aux abords des installations, d'établissements ou d'ouvrages sensibles".
Les perquisitions de nuit, jusqu'alors réservées aux juges, pourront désormais être ordonnées dans les enquêtes préliminaires du parquet, y compris dans les logements et même de façon préventive pour "prévenir un risque d'atteinte à la vie ou à l'intégrité physique".
INTERCEPTIONS
Le texte entend également donner au parquet et aux juges d'instruction l'accès à de nouvelles mesures d'investigation en matière de communication électronique et à de nouvelles techniques comme les "+Imsi-catcher+, qui interceptent les communications dans un périmètre donné en imitant le fonctionnement d'un relais téléphonique mobile".
TÉMOINS, FINANCEMENT ET BIENS CULTURELS
Le projet de loi vise aussi à mieux protéger les témoins avec des témoignages sous numéro et en prévoyant le recours au huis clos durant leur témoignage dans certains procès sensibles.
La lutte contre le financement du terrorisme sera également facilitée par un encadrement et une traçabilité des cartes prépayées, la possibilité pour Tracfin, organisme antiblanchiment du ministère de l'Économie, de signaler aux banques des opérations et des personnes à risque, et une extension du champ du gel des avoirs.
Enfin, une incrimination nouvelle visant à réprimer le trafic des biens culturels sera créée pour éviter que des groupes terroristes syriens ou libyens puissent "recycler sur notre sol le fruit du pillage du patrimoine de l'humanité".
Par Pierre ROCHICCIOLI 2016 AFP
Orange et Bouygues en pourparlers avancés, vous en pensez quoi ?
Ce sera, bien sûr, au bénéfice du consommateur et des salariés concernés ...
Ça tombe bien, je suis client ailleurs
Encore un petit effort et on va revenir aux PTT
Je n'ai pas de portable et je ne réponds jamais sur le fixe

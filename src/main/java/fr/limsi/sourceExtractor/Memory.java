package fr.limsi.sourceExtractor;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class Memory {
	public ConcurrentHashMap<String, String> parsedTexts;
	public ConcurrentHashMap<String, String> docTexts;
	public ConcurrentHashMap<String, ArrayList<Integer>> sentenceOffsetsByFile;
	public ConcurrentHashMap<String, String> revisionDate;

	public Memory(ConcurrentHashMap<String, String> parsedTexts, ConcurrentHashMap<String, String> docTexts,
			ConcurrentHashMap<String, ArrayList<Integer>> sentenceOffsetsByFile, ConcurrentHashMap<String, String> revisionDate) {
		this.parsedTexts = parsedTexts;
		this.docTexts = docTexts;
		this.sentenceOffsetsByFile = sentenceOffsetsByFile;
		this.revisionDate = revisionDate;
	}

	public void cleanEntry(String fileId) {
		this.parsedTexts.remove(fileId);
		this.docTexts.remove(fileId);
		this.sentenceOffsetsByFile.remove(fileId);
		this.revisionDate.remove(fileId);
	}

}

/**
 * Bertrand Goupil - AFP Medialab - Refactoring
 * Xavier Tannier - Limsi - Core coding
 */
package fr.limsi.sourceExtractor.training;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.tokensregex.MatchedExpression;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.util.CoreMap;
import fr.limsi.sourceExtractor.FeatureSet;
import fr.limsi.sourceExtractor.IOUtils;
import fr.limsi.sourceExtractor.Memory;
import fr.limsi.sourceExtractor.Resources;
import fr.limsi.sourceExtractor.Tools;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConstant;
import fr.limsi.sourceExtractor.application.configuration.SourceExtractorConfig;

public abstract class AProcessSupport {

	protected SourceExtractorConfig extractorConfig;

	public AProcessSupport(SourceExtractorConfig extractorConfig) {
		this.extractorConfig = extractorConfig;
	}

	public void deleteLastColumnForPrim(File outFilePRIM, File inFileSec, boolean testMode) throws IOException {
		Scanner scan = new Scanner(inFileSec);
		FileWriter fw = new FileWriter(outFilePRIM, true);
		BufferedWriter output = new BufferedWriter(fw);
		output.write("");

		StringBuilder sb = new StringBuilder();

		// We keep all fields to train secondary sources but for the primary
		// ones we need to remove the last column
		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			if (line.length() > 1) {
				String[] fields = line.split("\\s+");
				int size = fields.length;
				// we write each field but the last one.
				int limit = size - 1;
				// if in test mode, also remove the reference SOURCE-PRIM field
				// so that it will not be used by the SOURCE-SEC test
				if (testMode) {
					limit--;
				}
				for (int fieldIndex = 0; fieldIndex < limit; fieldIndex++) {
					sb.append(fields[fieldIndex] + " ");

				}
				sb.append("\n");
			} else {
				sb.append("\n");
			}
		}

		output.append(sb);
		output.close();
		scan.close();
	}

	public String tagAndConvert(String fileId, String text, File outFileSEC, String ann, Tools tools, Resources resources, Memory memory)
			throws IOException {
		// we use malt parser to keep the subjects found
		Annotation document = new Annotation(text);
		String maltParsertext = parseTextForMaltParser(document, text, tools, resources);

		// we parse the text to tag each word of this text.
		String parsedText = parseText(document, fileId, text, tools, resources, memory);

		// we join both parse text
		String taggedText = tagFromMaltParserAndParseText(parsedText, maltParsertext, resources);

		// we convert this text in (B)IO and we write the result into the file
		// outFileSEC
		bioConversion(taggedText, ann, outFileSEC);
		return parsedText;
	}

	/**
	 * Méthode qui va tag le texte passé en paramètre. Utilisée dans la fonction parseFileToTag qui récupère le résultat
	 * du tagging avec l'aide d'un StringBuilder.<br />
	 * <br />
	 * Nous utilisons le parser de Stanford CoreNLP pour donner le tag PoS à chaque mot du texte et pour donner le lemme
	 * qui va avec chaque mot.<br />
	 * <br />
	 * Nous utilisons ensuite une liste de verbes qui traduisent une action de citation (citVerbs) et pour chaque mot du
	 * texte nous regardons si c'est un mot<br />
	 * déclencheur (isTrigger). De plus nous ne gardons dans le StringBuilder que les phrases contenant un mot
	 * déclencheur (pour que le CRF ne s'entraine que<br />
	 * sur les phrases contenant une citation).<br />
	 * <br />
	 * Nous ajoutons à ce StringBuilder une fenêtre de mots par rapport aux guillemets trouvés dans les phrases. D'abord
	 * nous parcourons tout le texte en mettant<br />
	 * la position de chaque guillemet dans un HashSet quotePositions puis pour chaque w index et 2 * w index suivants
	 * (précédents), nous mettons un tag QM+ (QM- resp.) <br />
	 * à chacun dans un HashMap quoteTags. En parcourant, ensuite, le text quand on contruit le StringBuilder, si une
	 * clé de quoteTags contient l'index en cours<br />
	 * on écrit la valeur de cette clé dans le StringBuilder.<br />
	 * 
	 * Nous gérons une liste de professions ainsi qu'une liste de médias qui permettent d'ajouter 2 colonnes pour aider
	 * le CRF.<br />
	 * 
	 * Finalement le StringBuilder sera de la forme suivante : word PoS lemma isProf isTrigger quoteMark isMedia mix_col
	 * token.beginPosition():token.endPosition() token.beginPosition():token.endPosition()
	 * 
	 * 
	 * @param text
	 *            Le text à tag.
	 * @return Le StringBuilder créé avec toutes les informations nécessaires. (6 champs)
	 */
	protected String parseText(Annotation document, String fileId, String text, Tools tools, Resources resources, Memory memory) {
		String parsedText = memory.parsedTexts.get(fileId);

		if (parsedText != null) {
			return parsedText;
		}
		// create an empty Annotation just with the given text
		//Annotation document = new Annotation(text);

		// run all Annotators on this text
		tools.getCoreNLPParser().annotate(document);

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and
		// has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		StringBuilder result = new StringBuilder();

		HashSet<Integer> quotePositions = new HashSet<>();
		HashSet<Integer> finalQuotePositions = new HashSet<>();

		HashMap<Integer, String> quoteTags = new HashMap<>();

		int startIndex = 0;
		int indexToken;

		// Stanford has a problem with annotation of the quotes at the beginning
		// of a sentence. It puts the quote at the end of the previous sentence.
		// What we do is when a quotation mark is at after a newline, we put
		// that quote at the beginning of the next sentence.
		// At first we look for the position of these quotations and we keep
		// their position in a HashSet.
		while ((indexToken = text.indexOf("\n\"", startIndex)) != -1) {
			finalQuotePositions.add(indexToken + 1);
			startIndex = indexToken + 1;
		}

		int window = 5;
		int indexWord = 1;
		CoreLabel keptToken = null;

		ArrayList<Integer> sentenceOffsets = new ArrayList<>();

		for (CoreMap sentence : sentences) {
			int tokenPosition = 0;

			int idLine = 1;

			List<CoreLabel> tokens = sentence.get(TokensAnnotation.class);

			if (keptToken != null) {
				tokens.add(0, keptToken);
				keptToken = null;
			}

			for (CoreLabel token : tokens) {
				// Store start of sentence position
				if (tokenPosition == 0) {
					sentenceOffsets.add(token.beginPosition());
				}

				// this is the text of the token
				String word = token.get(TextAnnotation.class);

				boolean switchQuote = false;
				int beginPosition = token.beginPosition();

				// If the quotation mark is in the HashSet and if it is not the
				// first token of the sentence we switch the position of the
				// quotation.
				if (finalQuotePositions.contains(beginPosition) && token != tokens.get(0)) {
					switchQuote = true;
				}
				if (switchQuote) {
					assert tokenPosition == tokens.size() - 1 : "tokenPosition :" + tokenPosition + " != tokens.size() - 1 :" + (tokens.size() - 1) + "\n"
							+ "Values must be equal.";
					keptToken = token;
					continue;
				}

				if (word.equals("\"") || word.equals("»") || word.equals("«") || word.equals("“")) {
					quotePositions.add(tokenPosition);
				}

				tokenPosition++;

			}
			// We place a tag QM for each quotation mark found and +/- window
			// for each word located at a +/- distance of the quotation mark
			for (int quotePosition : quotePositions) {
				for (int index = quotePosition - (2 * window); index < quotePosition - window; index++) {
					quoteTags.put(index, "QM-" + 2 * window);
				}

				for (int index = quotePosition + window; index < quotePosition + 2 * window; index++) {
					quoteTags.put(index, "QM+" + 2 * window);
				}
			}

			for (int quotePosition : quotePositions) {
				for (int index = quotePosition - window; index < quotePosition; index++) {
					quoteTags.put(index, "QM-" + window);
				}
				for (int index = quotePosition; index < quotePosition + window; index++) {
					quoteTags.put(index, "QM+" + window);
				}
			}

			for (int quotePosition : quotePositions) {
				for (int index = quotePosition; index <= quotePosition; index++) {
					quoteTags.put(index, "QM");
				}
			}

			StringBuilder sentenceResult = new StringBuilder();

			boolean triggerFound = false;

			List<MatchedExpression> matchedExpressions = tools.triggerExtractor.extractExpressions(sentence);
			HashSet<Integer> triggerPositions = new HashSet<>();

			// we extract the trigger words for primary quotes and we keep their
			// position
			for (MatchedExpression expression : matchedExpressions) {
				for (int offset = expression.getCharOffsets().getBegin(); offset < expression.getCharOffsets().getEnd(); offset++) {
					triggerPositions.add(offset);
				}
				triggerFound = true;
			}

			// we extract the media words and we keep their position
			HashSet<Integer> triggerPositionsMedia = new HashSet<>();
			if (extractorConfig.isSearchSecondary()) {
				List<MatchedExpression> matchedExpressionsMedia = tools.mediaExtractor.extractExpressions(sentence);

				for (MatchedExpression expression : matchedExpressionsMedia) {
					for (int offset = expression.getCharOffsets().getBegin(); offset < expression.getCharOffsets().getEnd(); offset++) {
						triggerPositionsMedia.add(offset);
					}
					triggerFound = true;
				}
			}
			// we extract the trigger words for secondary quotes and we keep
			// their position
			List<MatchedExpression> matchedExpressionsTriggerSecondary = tools.triggerSecondaryExtractor.extractExpressions(sentence);
			HashSet<Integer> triggerPositionsSecondary = new HashSet<>();

			for (MatchedExpression expression : matchedExpressionsTriggerSecondary) {
				for (int offset = expression.getCharOffsets().getBegin(); offset < expression.getCharOffsets().getEnd(); offset++) {
					triggerPositionsSecondary.add(offset);
				}
				triggerFound = true;
			}

			tokenPosition = 0;
			// traversing the words in the current sentence
			// a CoreLabel is a CoreMap with additional token-specific methods
			for (CoreLabel token : tokens) {
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				// replace for example the phone numbers 06 xx xx xx xx with
				// 06_xx_xx_xx_xx in the text
				word = word.replaceAll(" ", "_");

				boolean switchQuote = false;
				int beginPosition = token.beginPosition();
				if (finalQuotePositions.contains(beginPosition) && token != tokens.get(0)) {
					switchQuote = true;
				}
				if (switchQuote) {
					assert tokenPosition == tokens.size() - 1 : "tokenPosition :" + tokenPosition + " != tokens.size() - 1 :" + (tokens.size() - 1)
							+ ", Ces valeurs doivent etre égales.";
					keptToken = token;
					continue;
				}

				// this is the POS tag of the token
				String pos = token.get(PartOfSpeechAnnotation.class);
				// this is the lemma of the token
				String lemma = tools.lemmatizer.getLemma(word, pos);

				// some corrections to help the CRF
				if (pos.equals("N")) {
					pos = "NC";
				}
				if (resources.PERSON_ABBR.contains(word)) {
					pos = "NPP";
				}
				if (resources.TEMPORAL_EXPRESSIONS.contains(word.toLowerCase())) {
					pos = "NC-TEMP";
				}
				if (word.equals("dit")) {
					lemma = "dire";
				}
				if (word.equals("péri")) {
					pos = "VPP";
					lemma = "périr";
				}

				// if (word.equals("Emmanuelle") && pos.equals("NC"))
				// pos = "NPP";
				//
				// if (word.equals("Manuel") && pos.equals("NC"))
				// pos = "NPP";
				//
				// if (word.equals("Valls") && pos.equals("NC"))
				// pos = "NPP";
				//
				// if (word.equals("Sénat"))
				// pos = "NC";
				//
				// if (word.equals("cheikh")) {
				// pos = "NC";
				// }
				// if (lemma.equals("ben")) {
				// pos = "NPP";
				// }
				// if (lemma.equals("bent")) {
				// pos = "NPP";
				// }

				String isTrigger = "NO";
				String isMedia = "NO_MEDIA";
				String mix_col = lemma;

				if (triggerPositions.contains(token.beginPosition())) {
					isTrigger = "TRIGGER";
				}

				if (triggerPositionsMedia.contains(token.beginPosition())) {
					if (!isGeographic(word, resources))
						isMedia = "IS_MEDIA";
				}

				// mix_col contains :
				// either SOURCE_SEC_TRIGGER if the word is in the trigger words
				// for secondary quotes
				// or either the pos tagging of the word if it is a "N" pos and
				// not a trigger word for secondary quotes
				// otherwise it is the lemma
				if (triggerPositionsSecondary.contains(token.beginPosition())) {
					mix_col = "SOURCE_SEC_TRIGGER";
				} else if (resources.COARSE_GRAINED_POS_MAP.get(pos).equals("N")) {
					mix_col = pos;
				}

				String quoteMark = "NQM";

				if (quoteTags.containsKey(tokenPosition)) {

					quoteMark = quoteTags.get(tokenPosition);

					if (quoteMark.equals("QM"))
						triggerFound = true;
				}

				if (resources.CITATION_VERBS.contains(lemma)) {
					isTrigger = "TRIGGER";
					triggerFound = true;
				}

				String isProf = "ISNOTPROF";
				if (resources.PROFESSION_WORDS.contains(word)) {
					isProf = "PROF";
				}
				// Two token.beginPosition() + ":" + token.endPosition() to make
				// easier the (B)IO conversion
				// Two sentenceResult result to make easier the coreferences.
				// if (!onlyOffset) {
				sentenceResult.append(
						indexWord + "-" + idLine + "\t" + word + "\t" + pos + "\t" + lemma + "\t" + isTrigger + "\t" + isProf + "\t" + quoteMark + "\t" + isMedia + "\t"
								+ mix_col + "\t" + token.beginPosition() + ":" + token.endPosition() + "\t" + token.beginPosition() + ":" + token.endPosition() + "\n");
				// }
				// else {
				// sentenceResult.append(word + "\t" + pos + "\t" + lemma + "\t"
				// + isProf + "\t"
				// + token.beginPosition() + ":" + token.endPosition() + "\n");
				// }

				idLine++;
				tokenPosition++;
			}
			sentenceResult.append("\n");

			if (triggerFound) {
				result.append(sentenceResult);
			}

			quotePositions.clear();
			quoteTags.clear();
			indexWord++;
		}
		String resultStr = result.toString();
		// if (!onlyOffset)
		memory.parsedTexts.put(fileId, resultStr);

		memory.sentenceOffsetsByFile.put(fileId, sentenceOffsets);

		return resultStr;
	}

	/**
	 * Méthode permettant de remplir 5 HashSets selon la source lue dans le fichier ann.<br>
	 * On note l'offset de début de chaque source primaire et secondaire dans leur HashSet respectif ainsi que tous les
	 * offsets jusqu'à la fin de la source.<br>
	 * 
	 * Pour l'annotation des coreferences, nous enregistrons aussi tous les offsets de chaque source par rapport à leur
	 * id.<br>
	 * sourceAndOffset sera de la forme : {T1 = [547,548,549,550,551,552,...]}<br>
	 * 
	 * @param listOfBratAnnotation
	 * @param name
	 */
	protected void fillMapsWithOffsets(List<String[]> listOfBratAnnotation, String name, HashSet<Integer> startOffsetsPrim, HashSet<Integer> inOffsetsPrim,
			HashSet<Integer> startOffsetsSec, HashSet<Integer> inOffsetsSec, HashMap<String, ArrayList<Integer>> sourceAndOffset) {

		// startOffsetsPrim = new HashSet<>();
		// inOffsetsPrim = new HashSet<>();
		// startOffsetsSec = new HashSet<>();
		// inOffsetsSec = new HashSet<>();
		// sourceAndOffset = new HashMap<>();

		for (String[] bratAnnotation : listOfBratAnnotation) {
			ArrayList<Integer> offsets = new ArrayList<>();
			if (bratAnnotation.length != 1) {
				String entity = bratAnnotation[1];
				String id = bratAnnotation[0];

				if (bratAnnotation.length <= 1) {
					System.out.println("Fichier d'annotation corrompu : " + name + ".ann.\nVoir cette ligne : \t" + Arrays.toString(bratAnnotation));
					System.exit(1);
				}

				int leftOffset = 0;
				int rightOffset = 0;

				switch (entity) {

				case SourceExtractorConstant.SOURCEPRIM:
					leftOffset = Integer.parseInt(bratAnnotation[2]);
					rightOffset = Integer.parseInt(bratAnnotation[3]);
					startOffsetsPrim.add(leftOffset);

					offsets.add(leftOffset);
					sourceAndOffset.put(id, offsets);

					for (int offset = leftOffset; offset < rightOffset; offset++) {
						inOffsetsPrim.add(offset);
						sourceAndOffset.get(id).add(offset);
					}

					break;

				case SourceExtractorConstant.SOURCESEC:
					leftOffset = Integer.parseInt(bratAnnotation[2]);
					rightOffset = Integer.parseInt(bratAnnotation[3]);
					offsets.add(leftOffset);
					sourceAndOffset.put(id, offsets);

					startOffsetsSec.add(leftOffset);
					for (int offset = leftOffset; offset < rightOffset; offset++) {
						inOffsetsSec.add(offset);
						sourceAndOffset.get(id).add(offset);
					}

					break;

				default:
					break;
				}

			}
		}
	}

	protected boolean isGeographic(String currentWord, Resources resources) {
		boolean isGeographic = resources.COUNTRIES_AND_CONTINENTS.contains(currentWord) || resources.CAPITALS.contains(currentWord);
		return isGeographic;
	}

	/**
	 * Parse text with Malt parser, then return mal results as string and write in the given outputfile
	 * @param text
	 * @param outFileMP
	 * @param tools
	 * @param resources
	 * @return
	 * @throws IOException
	 */
	private String parseTextForMaltParser(Annotation document, String text, Tools tools, Resources resources) throws IOException {
		// create an empty Annotation just with the given text
		//Annotation document = new Annotation(text);

		// run all Annotators on this text
		tools.coreNLPParser.annotate(document);
		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and
		// has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		StringBuilder builder = new StringBuilder();
		StringBuilder result = new StringBuilder();

		int startIndex = 0;
		int indexToken;
		HashSet<Integer> finalQuotePositions = new HashSet<>();

		while ((indexToken = text.indexOf("\n\"", startIndex)) != -1) {
			finalQuotePositions.add(indexToken + 1);
			startIndex = indexToken + 1;
		}

		int idLine = 1;
		CoreLabel keptToken = null;

		for (CoreMap sentence : sentences) {
			int tokenPosition = 0;

			int index = 1;
			List<CoreLabel> tokens = sentence.get(TokensAnnotation.class);

			if (keptToken != null) {
				tokens.add(0, keptToken);
				keptToken = null;
			}

			String malt = "";

			for (CoreLabel token : tokens) {
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				word = word.replaceAll(" ", "_");

				boolean switchQuote = false;
				int beginPosition = token.beginPosition();

				if (finalQuotePositions.contains(beginPosition) && token != tokens.get(0)) {
					switchQuote = true;
				}
				if (switchQuote) {
					if (tokenPosition != tokens.size() - 1) {
						System.out.println("tokenPosition :" + tokenPosition + " != tokens.size() - 1 :" + (tokens.size() - 1));
						System.out.println("Ces valeurs doivent etre égales.");
					}
					keptToken = token;
					continue;
				}

				String pos = token.get(PartOfSpeechAnnotation.class);
				String lemma = tools.lemmatizer.getLemma(word, pos);

				boolean isCL = pos.equals("CL");
				String supplementary = "_";
				if (pos.equals("PUNC"))
					pos = "PONCT";

				if (pos.equals("N")) {
					pos = "NC";
					supplementary = "NC";
				}

				if (resources.PERSON_ABBR.contains(word)) {
					pos = "NPP";
				}

				// some corrections to help malt parser and the CRF.
				if (pos.equals("C"))
					pos = "CS";

				if (word.equals("y") && isCL)
					pos = "CLO";

				if (word.equals("il") && isCL)
					pos = "CLS";

				if (word.equals("on") && isCL)
					pos = "CLS";

				if (word.equals("dit")) {
					lemma = "dire";
				}
				if (word.equals("péri")) {
					pos = "VPP";
					lemma = "périr";
				}
				// if (word.equals("cheikh")) {
				// pos = "NC";
				// }
				//
				// if (lemma.equals("ben")) {
				// pos = "NPP";
				// }

				// if (word.equals("Sénat"))
				// pos = "NC";
				//
				// if (word.equals("Emmanuelle") && pos.equals("NC"))
				// pos = "NPP";
				//
				// if (word.equals("Manuel") && pos.equals("NC"))
				// pos = "NPP";
				//
				// if (word.equals("Valls") && pos.equals("NC"))
				// pos = "NPP";

				String basicCategory = resources.COARSE_GRAINED_POS_MAP.get(pos);

				builder.append(idLine + "-" + index + "\t" + word + "\t" + lemma + "\t" + basicCategory + "\t" + pos + "\t" + supplementary + "\t\n");
				index++;
				tokenPosition++;
			}
			// we run malt parser on each sentence
			malt = runMaltParser(builder.toString(), tools);

			result.append(malt);
			builder.setLength(0);
			idLine++;
		}

		return result.toString();
	}

	/**
	 * Méthode permettant de rassembler les informations trouvées avec malt parser et avec le parse text.
	 * 
	 * @param parsedText
	 *            Texte tag provenant de la fonction parseText
	 * @param maltResult
	 *            Text provenant de la fonction parseTextForMaltParser
	 * @return Une jointure des deux textes
	 */
	private String tagFromMaltParserAndParseText(String parsedText, String maltResult, Resources resources) {
		StringBuilder sb = new StringBuilder();

		FeatureSet myText = null;

		Scanner parsedTextScanner = new Scanner(parsedText);
		String parsedTextLine;

		Scanner maltResultScanner = new Scanner(maltResult);
		String maltResultLine;

		HashMap<String, String> sameIndexForSubject = new HashMap<>();
		HashSet<String> triggerVerbIndices = new HashSet<>();

		// at first we loop through the result of malt parser
		while (maltResultScanner.hasNextLine()) {
			maltResultLine = maltResultScanner.nextLine();
			if (maltResultLine.length() > 1) {

				String[] malt = maltResultLine.split("\\s+");
				// this is the id of the current line
				String currentLine = malt[0];
				String lemma = malt[2];
				String pos = malt[3];
				// this is the syntactic relationship between HEAD and this word
				String deprel = malt[6];
				// this is the syntactic annotation of this word found by malt
				// parser
				String syntacticAnnotation = malt[7];

				String indexForDeprel = "";

				int indexOfCurrentLine = currentLine.lastIndexOf("-");

				if (indexOfCurrentLine > 0) {
					indexForDeprel = currentLine.substring(0, indexOfCurrentLine);
				}

				// if malt parser marked this word as a subject so we keep it
				if (syntacticAnnotation.equals("suj")) {
					sameIndexForSubject.put(currentLine, indexForDeprel + "-" + deprel);
				}

				// we keep each verbs which are contained in citVerbs
				if (pos.equals("V") && resources.CITATION_VERBS.contains(lemma)) {
					triggerVerbIndices.add(currentLine);
				}
			}
		}

		// Then we loop through the parsed text
		while (parsedTextScanner.hasNextLine()) {
			parsedTextLine = parsedTextScanner.nextLine();
			if (parsedTextLine.length() > 1) {

				String[] parsed = parsedTextLine.split("\\s+");

				String idLine = parsed[0];
				String word = parsed[1];
				String pos = parsed[2];
				String lemma = parsed[3];
				String isTrigger = parsed[4];
				String isProf = parsed[5];
				String QM = parsed[6];
				String isMedia = parsed[7];
				String mix_col = parsed[8];
				String offsetPrim = parsed[9];

				myText = new FeatureSet(word, pos, lemma, isTrigger, isProf, QM, isMedia, mix_col, offsetPrim, offsetPrim);

				// we check if malt parser marked this current line as a subject
				if (sameIndexForSubject.containsKey(idLine)) {
					myText.setIsSuj();
					// we also check if this subject is the subject of a
					// quotation verb
					if (triggerVerbIndices.contains(sameIndexForSubject.get(idLine))) {
						myText.setIsSujForTrigger();
					}
				}
				sb.append(myText + "\n");

			} else {
				sb.append("\n");
			}

		}

		maltResultScanner.close();
		parsedTextScanner.close();
		return sb.toString();
	}

	/**
	 * Méthode qui permet de transformer un String en un fichier (B)IO.<br />
	 * <br />
	 * Pour cela nous récupérons le String tagAndOffset qui a été tag par la fonction parseText et on le split par
	 * rapport aux espaces trouvés.<br />
	 * exemple : marche NC marche NO NQM O --------> [marche, NC, marche, NO, NQM, O].<br />
	 * <br />
	 * Nous splittons de la même façon le fichier annoté ann qui provient du dossier input et qui porte le même nom que
	 * le fichier dont le tagAndOffset est issu.<br />
	 * Après avoir split, nous regardons pour chaque ligne si l'annotation est une SOURCE-PRIM ou SOURCE-SEC, si oui on
	 * sauvegarde l'offset de début dans le HashSet startOffsets et<br />
	 * les offsets suivant jusqu'à l'offset de fin de la SOURCE-PRIM ou SOURCE-SEC dans un HashSet inOffsets.<br />
	 * <br />
	 * Nous parcourons ensuite le String taggedText ligne par ligne et pour chaque offset on vérifie s'il est contenu
	 * dans startOffsets (si oui on inscrit B à la place de l'offset), ou s'il est contenu dans inOffsets (si oui on
	 * inscrit I à la place de l'offset) ou s'il n'est ni dans l'un ni dans l'autre (si oui on inscrit O à la place de
	 * l'offset). Si aucune source n'est présente dans le fichier annoté (ou si justement pas encore annoté), on met
	 * directement O à la place de tous les offsets.<br />
	 * <br />
	 * Finalement on écrit le résultat dans le fichier outFile qui porte le même nom que le fichier d'entrée mais avec
	 * l'extension .tag.<br />
	 * <br />
	 * 
	 * @param taggedText
	 *            String qui a été tag par la fonction tagFromMaltParserAndParseText.
	 * @param ann
	 *            Contenu d'un fichier annoté qui provient du dossier input portant le même nom que le fichier d'entrée.
	 * @param outFile
	 *            Fichier où on écrit le résultat de la conversion.
	 * @return le résultat de la conversion.
	 * @throws IOException
	 */
	private String bioConversion(String taggedText, String ann, File outFile) throws IOException {
		String name = outFile.getName();
		int pos = name.lastIndexOf(".");
		if (pos > 0) {
			name = name.substring(0, pos);
		}

		List<String[]> listOfTextTag = new LinkedList<String[]>();
		String[] wordsTag = taggedText.split("\\r?\\n");

		for (int i = 0; i < wordsTag.length; i++) {
			listOfTextTag.add(wordsTag[i].split("\\s+"));
		}

		List<String[]> listOfBratAnnotation = new LinkedList<String[]>();
		String[] wordsAnn = ann.split("\\r?\\n");

		for (int i = 0; i < wordsAnn.length; i++) {
			listOfBratAnnotation.add(wordsAnn[i].split("\\s+"));
		}

		HashSet<Integer> startOffsetsPrim = new HashSet<>();
		HashSet<Integer> inOffsetsPrim = new HashSet<>();
		HashSet<Integer> startOffsetsSec = new HashSet<>();
		HashSet<Integer> inOffsetsSec = new HashSet<>();
		HashMap<String, ArrayList<Integer>> sourceAndOffset = new HashMap<>();

		fillMapsWithOffsets(listOfBratAnnotation, name, startOffsetsPrim, inOffsetsPrim, startOffsetsSec, inOffsetsSec, sourceAndOffset);

		writeBIO(startOffsetsPrim, inOffsetsPrim, listOfTextTag, SourceExtractorConstant.PRIM, name);

		writeBIO(startOffsetsSec, inOffsetsSec, listOfTextTag, SourceExtractorConstant.SEC, name);

		StringBuilder builder = new StringBuilder();

		for (String[] w : listOfTextTag)
			builder.append((String.join(" ", w) + "\n"));

		IOUtils.write(builder.toString(), outFile);

		return builder.toString();
	}

	/**
	 * clean and write malt result to output file and return string buffer
	 * @deprecated not sure about the use of the conll file. 
	 * @param maltParsertext
	 * @param outFileMP
	 * @param tools
	 * @return
	 * @throws IOException
	 */
	private String runMaltParser(String maltParsertext, File outFileMP, Tools tools) throws IOException {

		FileWriter fw = new FileWriter(outFileMP, true);
		BufferedWriter output = new BufferedWriter(fw);
		output.write("");

		String[] tokens = maltParsertext.split("\\r?\\n");
		StringBuilder sb = new StringBuilder();
		try {

			String[] outputTokens = tools.getMaltParserModel().parseTokens(tokens);

			for (int i = 0; i < outputTokens.length; i++) {
				sb.append(outputTokens[i] + "\t\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		output.append(sb);
		output.close();

		return sb.toString();
	}
	
	private String runMaltParser(String maltParsertext, Tools tools) throws IOException {
		String[] tokens = maltParsertext.split("\\r?\\n");
		StringBuilder sb = new StringBuilder();
		try {

			String[] outputTokens = tools.getMaltParserModel().parseTokens(tokens);

			for (int i = 0; i < outputTokens.length; i++) {
				sb.append(outputTokens[i] + "\t\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/**
	 * Méthode permettant de remplacer les offsets de fin de chaque ligne de tag par un encoding (B)IO.
	 * 
	 * @param startOffsets
	 *            HashSet qui donne le début de chaque source
	 * @param inOffsets
	 *            HashSet qui donne l'intérieur de chaque source
	 * @param listOfTextTag
	 *            La liste de tag d'un fichier
	 * @param suffix
	 *            PRIM ou SEC selon si on veut convertir la source PRIM ou SEC
	 * @param name
	 *            Nom du fichier en cours
	 */
	private void writeBIO(HashSet<Integer> startOffsets, HashSet<Integer> inOffsets, List<String[]> listOfTextTag, String suffix, String name) {

		// if there is a source to transform we look for the beginning of the
		// source, otherwise we know that we can put "O" at the end of each
		// line.
		if (startOffsets.size() > 0) {
			for (String[] fieldsTag : listOfTextTag) {

				if (fieldsTag.length != SourceExtractorConstant.numberFieldsSec + 1 && fieldsTag.length != 1) {
					System.out.println("Les fichiers avec offsets doivent avoir 11 champs, " + name + ".txt est corrompu\n" + "Voir cette ligne : \t"
							+ Arrays.toString(fieldsTag) + "et " + fieldsTag.length);
					System.exit(1);
				}

				try {
					// If it is not a new line
					if (fieldsTag.length != 1) {
						String[] offsetsStr = null;
						// If PRIM : fieldsTag[numberFieldsPrim] contains the
						// offset of the start and of the end of the current
						// word separated by ":"
						// If SEC : same thing but with the next fieldsTag.
						if (suffix.equals(SourceExtractorConstant.PRIM)) {
							offsetsStr = fieldsTag[SourceExtractorConstant.numberFieldsPrim].split(":");
						} else if (suffix.equals(SourceExtractorConstant.SEC)) {
							offsetsStr = fieldsTag[SourceExtractorConstant.numberFieldsSec].split(":");
						}

						int offsetLeft = Integer.parseInt(offsetsStr[0]);
						int offsetRight = Integer.parseInt(offsetsStr[1]);
						String className = null;

						// we look at each offset between the start and the end
						// of each word
						// and if the start of a source is equal to the current
						// offset then we put className to B or I (according to
						// the encoding wanted)
						for (int offset = offsetLeft; offset < offsetRight; offset++) {
							if (startOffsets.contains(offset)) {
								if (extractorConfig.isModelBio())
									className = SourceExtractorConstant.encodingB + suffix;
								else
									className = SourceExtractorConstant.encodingI + suffix;
								break;
							}
						}
						// Now we have already marked all starts of sources, we
						// then need to mark the offsets included in sources.
						if (className == null) {
							// we look at each offset between the start and the
							// end of each word that are not already marked
							for (int offset = offsetLeft; offset < offsetRight; offset++) {
								if (inOffsets.contains(offset)) {
									className = SourceExtractorConstant.encodingI + suffix;
									break;
								}
							}
						}
						// Here we are sure that all sources have already been
						// marked so we can transform everything else into "O"
						if (className == null) {
							className = SourceExtractorConstant.OUT;
						}
						// We replace the values of the offsets with the
						// className found.
						if (suffix.equals(SourceExtractorConstant.PRIM))
							fieldsTag[SourceExtractorConstant.numberFieldsPrim] = className;
						else if (suffix.equals(SourceExtractorConstant.SEC))
							fieldsTag[SourceExtractorConstant.numberFieldsSec] = className;
					}
				} catch (NumberFormatException e) {
					throw e;
				}
			}
		} else {
			for (String[] fieldsTag : listOfTextTag) {
				if (fieldsTag.length != 1) {
					if (suffix.equals(SourceExtractorConstant.PRIM))
						fieldsTag[SourceExtractorConstant.numberFieldsPrim] = SourceExtractorConstant.OUT;
					else if (suffix.equals(SourceExtractorConstant.SEC))
						fieldsTag[SourceExtractorConstant.numberFieldsSec] = SourceExtractorConstant.OUT;
				}
			}
		}
	}

}

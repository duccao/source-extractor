package fr.limsi.sourceExtractor.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class SourceExtractorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SourceExtractorApplication.class, args);
	}
}

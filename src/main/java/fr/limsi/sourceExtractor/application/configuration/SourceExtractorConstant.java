package fr.limsi.sourceExtractor.application.configuration;

import java.util.regex.Pattern;

public class SourceExtractorConstant {

	// Patterns
	public static final Pattern BRAT_ENTITY_PATTERN = Pattern.compile("^([^\\s]+)\t([^\\s]+) (.+)$");
	public static final Pattern PN_COMMA_PATTERN = Pattern.compile("^(\\p{javaUpperCase}[^, ]+( (de|du|\\p{javaUpperCase}[^, ]+))*) ?,.*");
	public static final Pattern COMMA_NP_AND_MORE_PATTERN = Pattern.compile("de [^()/,]+, (\\p{javaUpperCase}[^, ]+( (de|\\p{javaUpperCase}[^, ]+))+.*)$");
	public static final Pattern BRAT_ENTITY_DETAILED_PATTERN = Pattern.compile("^T(\\d+)\t([^\\s]+) (\\d+) (\\d+)\t(.+)$");
	public static final Pattern BRAT_COMMENT_DETAILED_PATTERN = Pattern.compile("#\\d+\tAnnotatorNotes T(\\d+)\t(.+)$");

	// Constants
	public final static String SOURCEPRIM = "SOURCE-PRIM";
	public final static String SOURCESEC = "SOURCE-SEC";
	public final static String SECONDARY = "secondary";
	public final static String PRIM = "-PRIM";
	public final static String SEC = "-SEC";
	public final static String OUT = "O";
	public final static String NULL = "NULL";
	public final static String SOURCE = "SOURCE";

	public final static String MODEL_PRIM = "model_primary";
	public final static String MODEL_SEC = "model_secondary";

	public static final String encodingI = "I";
	public static final String encodingB = "B";

	public static final int numberFieldsPrim = 10;
	public static final int numberFieldsSec = 11;
}

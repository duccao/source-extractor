package fr.limsi.sourceExtractor;

import java.io.File;

public class Paths {
	public File DATA_LABELED_DIR = null;
	public File DATA_UNLABELED_DIR = null;

	public File DIR_INPUT_LABELED = null;
	public File DIR_INPUT_LABELED_CONVERTED = null;

	public File DIR_INPUT_LABELED_MP = null;
	public File DIR_OUTPUT_LABELED = null;

	public File DIR_INPUT_UNLABELED = null;
	public File DIR_OUTPUT_UNLABELED = null;
	public File DIR_INPUT_UNLABELED_MP = null;

	public File DIR_INPUT_FINAL = null;
	public File DIR_OUTPUT_FINAL = null;
	
	public File DIR_TRAIN_FILES_PRIM = null;
	public File DIR_TEST_FILES_PRIM = null;
	public File DIR_DEV_FILES_PRIM = null;

	public File DIR_TRAIN_FILES_SEC = null;
	public File DIR_TEST_FILES_SEC =null;
	public File DIR_DEV_FILES_SEC = null;

	public File DIR_TRAIN_FILES_UNLABELED_PRIM = null;
	public File DIR_TEST_FILES_UNLABELED_PRIM = null;
	public File DIR_DEV_FILES_UNLABELED_PRIM = null;

	public File DIR_TRAIN_FILES_UNLABELED_SEC = null;
	public File DIR_TEST_FILES_UNLABELED_SEC = null;
	public File DIR_DEV_FILES_UNLABELED_SEC = null;

	public File DIR_RESULT_FILES_LABELED = null;
	public File DIR_MERGED_FILES_LABELED = null;

	public File TRAIN_PRIM_MERGED_LABELED = null;
	public File DEV_PRIM_MERGED_LABELED = null;

	public File TRAIN_SEC_MERGED_LABELED = null;
	public File DEV_SEC_MERGED_LABELED = null;

	public File DIR_MODELS_LABELED = null;

	public File PATTERN_PRIM = null;
	public File PATTERN_SEC = null;

	public File DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_PRIM = null;
	public File DIR_BIO_FILES_FOR_CONVERSION_WAPITI_LABELED_SEC = null;

	public File DIR_BRAT_AUTO_CONVERSION_WAPITI_LABELED = null;

	public File DIR_BRAT_REFERENCE_LABELED = null;

	public File DIR_FINAL_RESULT_LABELED = null;

	public File DIR_RESULT_FILES_UNLABELED = null;

	public File DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_PRIM = null;
	public File DIR_BIO_FILES_FOR_CONVERSION_WAPITI_UNLABELED_SEC = null;

	public File DIR_MERGED_FILES_UNLABELED = null;

	public File TRAIN_PRIM_MERGED_UNLABELED = null;
	public File DEV_PRIM_MERGED_UNLABELED = null;

	public File TRAIN_SEC_MERGED_UNLABELED = null;
	public File DEV_SEC_MERGED_UNLABELED = null;

	public File DIR_BRAT_AUTO_CONVERSION_WAPITI_UNLABELED = null;

	public File DIR_FINAL_RESULT_UNLABELED = null;

	public File MODEL_FINAL_DIR = null;
	
	public Paths() {
		
	}

}

package fr.limsi.sourceExtractor;

import org.maltparser.concurrent.ConcurrentMaltParserModel;

import edu.stanford.nlp.ling.tokensregex.CoreMapExpressionExtractor;
import edu.stanford.nlp.ling.tokensregex.MatchedExpression;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class Tools {
	// CoreNLP MWE extractors
	public CoreMapExpressionExtractor<MatchedExpression> triggerExtractor;
	public CoreMapExpressionExtractor<MatchedExpression> mediaExtractor;
	public CoreMapExpressionExtractor<MatchedExpression> triggerSecondaryExtractor;

	public StanfordCoreNLP coreNLPParser;
	public FrenchLemmatizer lemmatizer;
	protected ConcurrentMaltParserModel maltParserModel = null;
	
	
	public Tools(CoreMapExpressionExtractor<MatchedExpression> triggerExtractor,
			CoreMapExpressionExtractor<MatchedExpression> mediaExtractor,
			CoreMapExpressionExtractor<MatchedExpression> triggerSecondaryExtractor, StanfordCoreNLP coreNLPParser,
			FrenchLemmatizer lemmatizer, ConcurrentMaltParserModel maltParserModel) {
		super();
		this.triggerExtractor = triggerExtractor;
		this.mediaExtractor = mediaExtractor;
		this.triggerSecondaryExtractor = triggerSecondaryExtractor;
		this.coreNLPParser = coreNLPParser;
		this.lemmatizer = lemmatizer;
		this.maltParserModel = maltParserModel;
	}
	public CoreMapExpressionExtractor<MatchedExpression> getTriggerExtractor() {
		return triggerExtractor;
	}
	public void setTriggerExtractor(CoreMapExpressionExtractor<MatchedExpression> triggerExtractor) {
		this.triggerExtractor = triggerExtractor;
	}
	public CoreMapExpressionExtractor<MatchedExpression> getMediaExtractor() {
		return mediaExtractor;
	}
	public void setMediaExtractor(CoreMapExpressionExtractor<MatchedExpression> mediaExtractor) {
		this.mediaExtractor = mediaExtractor;
	}
	public CoreMapExpressionExtractor<MatchedExpression> getTriggerSecondaryExtractor() {
		return triggerSecondaryExtractor;
	}
	public void setTriggerSecondaryExtractor(CoreMapExpressionExtractor<MatchedExpression> triggerSecondaryExtractor) {
		this.triggerSecondaryExtractor = triggerSecondaryExtractor;
	}
	public StanfordCoreNLP getCoreNLPParser() {
		return coreNLPParser;
	}
	public void setCoreNLPParser(StanfordCoreNLP coreNLPParser) {
		this.coreNLPParser = coreNLPParser;
	}
	public FrenchLemmatizer getLemmatizer() {
		return lemmatizer;
	}
	public void setLemmatizer(FrenchLemmatizer lemmatizer) {
		this.lemmatizer = lemmatizer;
	}
	public ConcurrentMaltParserModel getMaltParserModel() {
		return maltParserModel;
	}
	public void setMaltParserModel(ConcurrentMaltParserModel maltParserModel) {
		this.maltParserModel = maltParserModel;
	}

	
}

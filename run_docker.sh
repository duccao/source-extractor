option=$1

if [ "$option" == "evaluate" ]; then
    input_folder=$2
    sudo rm -rf data
    sudo mkdir -p data/labeled
    sudo cp -r $input_folder data/labeled
    sudo docker run -v `pwd`/data:/default/data \
        source-extractor evaluate 
    python3 evaluate.py
fi

if [ "$option" == "extract" ]; then
    input_folder=$2
    output_folder=${3:-data/out}
    num_threads=${4:-4}

    sudo rm -rf data
    sudo mkdir -p data
    sudo cp -r $input_folder data
    sudo docker run -v `pwd`/data:/default/data \
        source-extractor extract data/$input_folder $output_folder $num_threads 
fi

